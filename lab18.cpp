﻿// lab18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

using namespace std;

class Kingdom
{
public:
    string m_name{};
    string m_domen{};
    int m_age{};

    const string& getName() const { return m_name; }
    const string& getDomen() const { return m_domen; }
    int getAge() const { return m_age; }

    Kingdom(const string& name = "", const string& domen = "", int age = 0)
        : m_name{ name }, m_domen{ domen }, m_age{ age }
    {
    }
};

class Animal : public Kingdom
{
public:
    long m_numbering{};
    string m_btype{};
    string m_stype{};

    Animal(long numbering = 0, const string& btype = "", const string& stype = "")
        : m_numbering{ numbering }, m_btype{ btype }, m_stype{ stype }
    {
    }

    void printNameAndNumbering() const
    {
        cout << m_name << ":  " << m_numbering << '\n';
        cout << "The biggest animal: " << m_btype << '\n';
        cout << "The smallest animal: " << m_stype << '\n';
    }

};

class Birds : public Kingdom
{
public:
    long m_numbering{};
    string m_btype{};
    string m_stype{};

    Birds(long numbering = 0, const string& btype = "", const string& stype = "")
        : m_numbering{ numbering }, m_btype{ btype }, m_stype{ stype }
    {
    }

    void printNameAndNumbering() const
    {
        cout << m_name << ":  " << m_numbering << '\n';
        cout << "The biggest bird: " << m_btype << '\n';
        cout << "The smallest bird: " << m_stype << '\n';
    }

};

class People : public Animal
{
public:
    long m_numbering{};
    string m_btype{};
    string m_stype{};

    People(long numbering = 0, const string& btype = "", const string& stype = "")
        : m_numbering{ numbering }, m_btype{ btype }, m_stype{ stype }
    {
    }

    void printNameAndNumbering() const
    {
        cout << m_name << ":  " << m_numbering << '\n';
        cout << "The biggest person: " << m_btype << '\n';
        cout << "The smallest person: " << m_stype << '\n';
    }

};


int main()
{
    Animal animal{ 1600000 };
    animal.m_name = "Animal";
    animal.m_btype = "Blue Whale";
    animal.m_stype = "Kitty Mouse";
    animal.printNameAndNumbering();
    cout << "============" << endl;

    Birds birds{ 19883 };
    birds.m_name = "Birds";
    birds.m_btype = "Vultur pryphus";
    birds.m_stype = "Mellisuga helenae";
    birds.printNameAndNumbering();
    cout << "============" << endl;

    People people{ 70000000 };
    people.m_name = "Population";
    people.m_btype = "Siltan Kesen";
    people.m_stype = "Khagendra Thapa Magar";
    people.printNameAndNumbering();
    cout << "============" << endl;

    return 0;
}